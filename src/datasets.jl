using DataFrames
using CSV

abstract type Dataset end
struct KarlsruheStudents <: Dataset end

"""
Function for loading data, dispatch on subtype of Dataset
The idea is that the underlying dataset may have any shape,
but the user does not have to concern themselves with that.
@return DataFrame
 :query - always contains the SQL query
 :label - symbol to match similar queries
 :parsed - true if sql is parseable, false otherwise
"""
function load_data(::Type{KarlsruheStudents})
    path = "data/karlsruhe/GtExamLog.tsv"
    data = CSV.read(path, DataFrame, delim='\t')
    data = data[data.PARSED .>= 0, :]
    rename!(data, Dict(:STATEMENT => :query, :TASKNUM => :label))
    return data
end
load_data(d) = throw(DomainError(d, "Unknown dataset $(d)"))
