"""
Set of functions that compute similarity between two SQL queries
"""
module QuerySim

export SqlQuery, features, jaccard
export load_data, Dataset, KarlsruheStudents

using PyCall
include("datasets.jl")

function __init__()
    py"""
    import sqlparse
    """
end

struct SqlQuery
    raw::String
    cst # concrete syntax tree
end

function SqlQuery(raw::String)
    cst = py"sqlparse.parse"(raw)[1]
    if ispynull(cst)
        cst = nothing
    end
    SqlQuery(raw, cst)
end

"""Takes two SQL queries as raw strings and returns a similarity between them"""
function query_sim(q1::String, q2::String)
    sq1 = SqlQuery(q1)
    sq2 = SqlQuery(q2)
    fd1, fa1 = features(sq1.cst, 0, [])
    fd2, fa2 = features(sq2.cst, 0, [])
    jaccard(vcat(fd1, fa1), vcat(fd2, fa2))
end

function features(raw::String)
    feats = []
    query = SqlQuery(raw)

    features(query.cst, 0, feats)
    return Set(feats)
end
"""
Each node of the concrete syntax tree (CST) is an instance of a subclass of Token
Python types cannot be used with multiple dispatch AFAIK, so we need to do manual checks
Sadly, parsing SQL is a bit too complicated. To do it properly, we would have to cosnider symbol tables
for aliases, join semantics etc. 
"""
function features(::Nothing, idx, feats) return feats end
function features(node::PyObject, idx, feats)
    # println(pytypeof(node), ", ", node.value)
    if pyisinstance(node, py"sqlparse.sql.Identifier")
        id_name = node.get_real_name()
        push!(feats, id_name)
        return feats # return early - TODO possibly extend to handle aliases (requires symbol table)
    end
    if pyisinstance(node, py"sqlparse.sql.TokenList")
        # If the node is instance of TokenList, go through its children
        for token in node.tokens
            if ~token.is_whitespace
                feats = features(token, 0, feats)
            end
        end
    end
    return feats
end

"Computes Jaccard similarity index between two sets"
function jaccard(set_a::AbstractSet{T}, set_b::AbstractSet{T}) where T
    return length(intersect(set_a, set_b)) / length(union(set_a, set_b))
end

end # module
