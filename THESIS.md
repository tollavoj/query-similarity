# Similarity of Analytical Queries and Reports in a Semantic Layer <!-- omit in toc -->
**Diploma Thesis**

Author: Vojtěch Tollar

## Description
It has been observed that the semantic layer in an analytical project
becomes hard to maintain after a while. The reason is that the project
contains many similar or identical analytical queries and reports.
Users tend to repeatedly create similar analytical queries and
reports, because they are not familiar with the entire semantic layer
in detail. These duplicates may be caused by two different users or
even one user over a long period of time. The project is going to
focus on the semantic layer defined by Multidimensional Analytical
Query Language (MAQL) and SQL.

This project has two goals
1) Provide heuristics to detect similarities in analytical queries and
reports
2) Provide tooling that is going to help users to use already
existing queries and reports instead of building new ones.

## Requirements

Provide an overview of the state-of-the-art methods in similarity
detection in SQL queries.
Propose a methodology for similarity detection which would fit the
target domain and implement selected algorithms.
Evaluate implemented algorithms. Use artificial as well as real
project data during the evaluation.
