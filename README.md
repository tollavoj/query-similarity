# SQL query similarity
## Scientific programming in Julia final project

Author: Vojtěch Tollar

This project attempts to implement a function that would measure
similarity between two SQL queries.

I have decided to implement a feature-based function. For each query,
a set of features is extracted from them, in this case the tables / datasets they
use and the attributes which are used for filtering.

To extract these features, an SQL parser is needed. I have not found a native Julia one,
which was being maintained and creating one from scratch is a bit too much for now,
so I decided that sqlparse python library would be the best choice, called using PyCall.

As a result, python and sqlparse library are required.
