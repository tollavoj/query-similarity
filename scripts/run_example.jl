using QuerySim
using Clustering

# load data from Karslruhe university database system exam
dataset = load_data(KarlsruheStudents)
# use only parseable data -> cleaner, but not necessary
dataset = filter(:PARSED => p -> p == 1, dataset)
# compute the features from each query
feats = features.(dataset.query)
# compute the distance matrix
dists = [1 - jaccard(feats[i], feats[j]) for i in eachindex(feats), j in eachindex(feats)]
# compute clusters using hierarchical clustering
dendrogram = hclust(dists)
clusters = cutree(dendrogram, k=4)
# check accuracy of the clusters
# note: accuracy is way overstated if only parseable data is allowed
acc = vmeasure(clusters, dataset.label)

# print a cluster
dataset.query[clusters .== 2]
