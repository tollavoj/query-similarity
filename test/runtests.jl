using QuerySim
using Test

@testset "jaccard similarity" begin
    @test jaccard(Set(["Employees", "department"]), Set(["department", "Employees"])) ≈ 1.0
    @test jaccard(Set(["Employees", "department"]), Set(["Employees", "startdate"])) ≈ 1.0 / 3.0
    @test jaccard(Set(["Employees", "department"]), Set(["Employers", "startdate"])) ≈ 0.0
end

qs = [
    "SELECT * FROM Employees AS E WHERE E.department = 'sales'",
    "SELECT * FROM Employees AS E WHERE E.department = 'store'",
    "SELECT * FROM Employees AS E WHERE E.startdate > '01/12/2015'",
]

@testset "SQL query features" begin
    @test issetequal(features(qs[1]), Set(["Employees", "department"])) 
    @test issetequal(features(qs[2]), Set(["Employees", "department"])) 
    @test issetequal(features(qs[3]), Set(["Employees", "startdate"])) 
end
